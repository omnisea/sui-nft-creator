module omnisea::nft_creator_test_4 {
    use sui::url::{Self, Url};
    use std::string;
    use sui::object::{Self, ID, UID};
    use sui::event;
    use sui::transfer;
    use sui::tx_context::{Self, TxContext};
    use sui::vec_map::VecMap;
    use sui::vec_map;
    use std::vector;
    use sui::erc721_metadata::{Self, ERC721Metadata};
    use sui::coin::{Self, Coin};
    use sui::sui::SUI;

    /// There isn't enough balance to mint
    const EMINT_NOT_ENOUGH_BALANCE: u64 = 1;

    /// Current time is before mint "from" date
    const EMINT_BEFORE_MINT: u64 = 2;

    /// Current time is after mint "to" date
    const EMINT_AFTER_MINT: u64 = 3;

    /// There is not equal number/length of submitted allowed addresses and related mints amount "vectors" or > max supply
    const EMINT_NOT_VALID_ALLOWLIST: u64 = 4;

    /// Minting user (receiver) not allowlisted or minting more than allocated
    const EMINT_NOT_ALLOWLISTED: u64 = 5;

    /// Supply exceeded
    const EMINT_MAX_SUPPLY_EXCEEDED: u64 = 6;

    /// Not deployer
    const EROLE_NOT_DEPLOYER: u64 = 7;

    /// Protocol fee exceeded limit
    const EINVALID_NEW_FEE: u64 = 8;

    struct Royalty has copy, drop, store {
        royalty_points_numerator: u64,
        payee_address: address,
    }

    struct MintedByCount has copy, drop, store {
        count: u64,
    }

    struct Collection has key, store {
        id: UID,
        owner: address,
        name: string::String,
        description: string::String,
        price: u64,
        from: u64,
        to: u64,
        maximum_supply: u64,
        total_supply: u64,
        uri: string::String,
        tokens_uri: string::String,
        royalty: Royalty,
        allowlist: Allowlist,
        first_token_id: u64,
    }

    struct ERC721 has key, store {
        id: UID,
        token_id: u64,
        name: string::String,
        uri: Url,
        metadata: ERC721Metadata,
    }

    // ===== Events =====

    struct NFTMinted has copy, drop, store {
        object_id: ID,
        receiver: address,
        name: string::String,
    }

    struct LaunchpadMetadata has key {
        id: UID,
        minted_by: VecMap<CollectionIdentifier, VecMap<address, MintedByCount>>,
        created_by: VecMap<address, VecMap<string::String, Collection>>,
    }

    struct Treasury has key, store {
        id: UID,
        location: address,
    }

    struct LaunchpadConfig has key {
        id: UID,
        deployer: address,
        treasury: Treasury,
        protocol_fee: u64,
    }

    struct Allowlist has key, store {
        id: UID,
        addresses: vector<address>,
        max_per_address: u64,
        is_enabled: bool,
        public_from: u64,
        public_max_per_address: u64,
    }

    struct CollectionIdentifier has copy, store, drop {
        name: string::String,
        creator: address,
    }

    fun init(ctx: &mut TxContext) {
        let deployer = tx_context::sender(ctx);

        transfer::share_object(LaunchpadMetadata {
            id: object::new(ctx),
            created_by: vec_map::empty(),
            minted_by: vec_map::empty(),
        });

        let treasury = Treasury {
            id: object::new(ctx),
            location: deployer,
        };
        transfer::share_object(LaunchpadConfig {
            id: object::new(ctx),
            deployer,
            treasury,
            protocol_fee: 2,
        });
    }

    public entry fun create_collection(
        om_launchpad: &mut LaunchpadMetadata,
        name: vector<u8>,
        description: vector<u8>,
        collection_uri: vector<u8>,
        tokens_uri: vector<u8>,
        price: u64, // In mists (9 decimals)
        from: u64,
        to: u64,
        maximum_supply: u64,
        royalty_points_numerator: u64,
        first_token_id: u64,
        ctx: &mut TxContext
    ) {
        let creator = tx_context::sender(ctx);

        let contract_uri = &mut string::utf8(b"ipfs://");
        string::append(contract_uri, string::utf8(collection_uri));

        // TODO (Must) Assert creator->name Collection doesn't exist!

        let collection = Collection {
            id: object::new(ctx),
            owner: creator,
            name: string::utf8(name),
            description: string::utf8(description),
            uri: *contract_uri,
            tokens_uri: string::utf8(tokens_uri),
            price,
            from,
            to,
            maximum_supply,
            total_supply: 0,
            royalty: Royalty { payee_address: creator, royalty_points_numerator },
            allowlist: Allowlist {
                id: object::new(ctx),
                addresses: vector::empty(),
                is_enabled: false,
                max_per_address: 0,
                public_from: 0,
                public_max_per_address: 0,
            },
            first_token_id,
        };
        let created_by = &mut om_launchpad.created_by;

        if (vec_map::contains(created_by, &creator)) {
            let creator_collections = vec_map::get_mut(created_by, &creator);
            vec_map::insert(creator_collections, collection.name, collection);
        } else {
            let created_by_collections = vec_map::empty<string::String, Collection>();
            vec_map::insert(&mut created_by_collections, collection.name, collection);
            vec_map::insert(created_by, creator, created_by_collections);
        };
        let minted_by_table = &mut om_launchpad.minted_by;
        vec_map::insert(minted_by_table, CollectionIdentifier { creator, name: string::utf8(name) }, vec_map::empty());
    }

    public entry fun mint(
        launchpad_config: &mut LaunchpadConfig,
        om_launchpad: &mut LaunchpadMetadata,
        collection_name: string::String,
        creator: address,
        amount: u64,
        asset: Coin<SUI>,
        ctx: &mut TxContext,
    ) {
        let sender = tx_context::sender(ctx);
        let balance = coin::value(&asset);
        let created_by = &mut om_launchpad.created_by;
        let creator_collections = vec_map::get_mut(created_by, &creator);
        let collection = vec_map::get_mut(creator_collections, &collection_name);
        let paid_amount = collection.price * amount;

        if (paid_amount > 0) {
            assert!(balance >= paid_amount, EMINT_NOT_ENOUGH_BALANCE);
        };

        let tokens_uri = collection.tokens_uri;

        if (collection.maximum_supply > 0) {
            assert!((amount + collection.total_supply) <= collection.maximum_supply, EMINT_MAX_SUPPLY_EXCEEDED);
        };

        let allowlist = &mut collection.allowlist;
        let epoch = tx_context::epoch(ctx);
        let minted_by_table = &mut om_launchpad.minted_by;
        let collection_identifier = CollectionIdentifier {
            creator,
            name: collection_name,
        };
        let collection_minted_by_table = vec_map::get_mut(minted_by_table, &collection_identifier);
        let minted_by_address = if (vec_map::contains(collection_minted_by_table, &sender)) {
            vec_map::get_mut(collection_minted_by_table, &sender)
        } else {
            &mut MintedByCount {
                count: 0,
            }
        };
        let new_receiver_balance = (minted_by_address.count + amount);

        if (allowlist.is_enabled) {
            if (epoch < allowlist.public_from) {
                if (allowlist.max_per_address > 0) {
                    let (is_allowlisted, _) = vector::index_of(&allowlist.addresses, &sender);
                    assert!(is_allowlisted, EMINT_NOT_ALLOWLISTED);
                    assert!(new_receiver_balance <= allowlist.max_per_address, EMINT_NOT_ALLOWLISTED);
                };
            } else if (allowlist.public_max_per_address > 0) {
                assert!(new_receiver_balance <= allowlist.public_max_per_address, EMINT_NOT_ALLOWLISTED);
            };
        };

        if (paid_amount > 0) {
            let fee_amount: u64 = launchpad_config.protocol_fee * paid_amount / 100;
            let paid_extracted_fee: u64 = paid_amount - fee_amount;

            let coin_paid_to_creator = coin::split(&mut asset, paid_extracted_fee, ctx);
            transfer::transfer(coin_paid_to_creator, collection.owner);

            if (fee_amount > 0) {
                let coin_fee = coin::split(&mut asset, fee_amount, ctx);
                transfer::transfer(coin_fee, launchpad_config.treasury.location);
            };
        };
        transfer::transfer(asset, sender);

        let new_total_supply = (collection.total_supply + amount);
        let last_minted_token_id = if (collection.first_token_id > 0) {
            new_total_supply + 1
        } else {
            new_total_supply
        };

        let index = collection.total_supply + collection.first_token_id;
        while (index < last_minted_token_id) {
            let index_vector = u64_to_string(index);
            // create a token data id to specify which token will be minted
            let token_uri = &mut string::utf8(b"ipfs://");
            string::append(token_uri, tokens_uri);
            string::append(token_uri, string::utf8(b"/"));
            string::append(token_uri, string::utf8(index_vector));
            string::append(token_uri, string::utf8(b".json"));

            let token_name = collection.name;
            string::append(&mut token_name, string::utf8(b" #"));
            string::append(&mut token_name, string::utf8(index_vector));

            let erc721_token_id = erc721_metadata::new_token_id(index);
            let token_uri = *string::bytes(token_uri);

            let nft = ERC721 {
                id: object::new(ctx),
                token_id: index,
                name: token_name,
                uri: url::new_unsafe_from_bytes(token_uri),
                metadata: erc721_metadata::new(erc721_token_id, *string::bytes(&token_name), token_uri),
            };

            event::emit(NFTMinted {
                object_id: object::id(&nft),
                receiver: sender,
                name: nft.name,
            });

            transfer::transfer(nft, sender);

            index = index + 1;
        };
        collection.total_supply = collection.total_supply + amount;
        minted_by_address.count = new_receiver_balance;
    }

    public entry fun set_allowlist(
        collection: &mut Collection,
        allowed_addresses: vector<address>,
        max_per_address: u64,
        public_max_per_address: u64,
        public_from: u64,
        is_enabled: bool,
        _: &mut TxContext
    ) {
        let allowlist = &mut collection.allowlist;
        allowlist.addresses = allowed_addresses;
        allowlist.max_per_address = max_per_address;
        allowlist.public_max_per_address = public_max_per_address;
        allowlist.public_from = public_from;
        allowlist.is_enabled = is_enabled;
    }

    public entry fun set_protocol_fee(
        launchpad_config: &mut LaunchpadConfig,
        new_protocol_fee: u64,
        ctx: &mut TxContext
    ) {
        let sender = tx_context::sender(ctx);
        assert!(sender == launchpad_config.deployer, EROLE_NOT_DEPLOYER);
        assert!(new_protocol_fee <= 5, EINVALID_NEW_FEE);
        launchpad_config.protocol_fee = new_protocol_fee;
    }

    public entry fun set_treasury_location(
        launchpad_config: &mut LaunchpadConfig,
        new_treasury_location: address,
        ctx: &mut TxContext
    ) {
        let sender = tx_context::sender(ctx);
        assert!(sender == launchpad_config.deployer, EROLE_NOT_DEPLOYER);
        launchpad_config.treasury.location = new_treasury_location;
    }

    fun u64_to_string(num: u64): vector<u8> {
        let v1 = vector::empty();

        while (num / 10 > 0) {
            let rem = num % 10;
            vector::push_back(&mut v1, (rem + 48 as u8));
            num = num / 10;
        };

        vector::push_back(&mut v1, (num + 48 as u8));
        vector::reverse(&mut v1);

        v1
    }
}
